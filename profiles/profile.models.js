const { sequelize, Sequelize,} = require("../db/models");

const user_bios = sequelize.define(
  "user_bios",
  {
    fullname: {
      type:Sequelize.STRING,
      allowNull: true
    },
    address:{
      type : Sequelize.STRING,
      allowNull:true
    },
    phoneNumber: {
      type : Sequelize.STRING,
      allowNull:true
    },
    dateOfBirth: {
      type:Sequelize.STRING,
      allowNull:true
    },
    userId:{
      type:Sequelize.INTEGER,
      allowNull:false,
      references: {
        model: "user_games",
        key: "id",
      },
    }
  },
  {
    timestamps: true,
  }
);

const user_games = sequelize.define(
  "user_games",
  {
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING,
  },
  {
    timestamps: true,
  }
);

user_games.hasOne(user_bios,{foreignKey:"userId"});
user_bios.belongsTo(user_games,{as:"user",foreignKey:"userId"})

class ProfileModel {
  getUserBiodata = async (userUserId) => {
    try {
      const data = await user_bios.findOne({
        attributes: [
          "fullname",
          "address",
          "phoneNumber",
          "dateOfBirth",
        ],
        include: [
          {
            model: user_games,
            as: "user",
            attributes: [["id", "userId"], "username"],
          },
        ],
        where: { userId:userUserId },
      });
      return data;
    } catch (error) {
      return undefined;
    }
  };

  findBioById = async (userId) => {
    try {
      const data = await user_bios.findOne({ where: { userId } });
      return data;
    } catch (error) {
      return undefined;
    }
  };

  createBiodata = async (
    userFullname,
    userAddress,
    userPhoneNumber,
    userDateOfBirth,
    userUserId
  ) => {
    try {
      await user_bios.create({
        fullname: userFullname,
        address: userAddress,
        phoneNumber: userPhoneNumber,
        dateOfBirth: userDateOfBirth,
        userId: userUserId,
      });
      return true;
    } catch (error) {
      return error;
    }
  };

  updateBiodata = async (
    userFullname,
    userAddress,
    userPhoneNumber,
    userDateOfBirth,
    userUserId
  ) => {
    try {
      await user_bios.update(
        { fullname:userFullname, address:userAddress, phoneNumber:userPhoneNumber, dateOfBirth:userDateOfBirth },
        { where: { userId:userUserId } }
      );
      return true;
    } catch (error) {
      return error;
    }
  };
}

module.exports = new ProfileModel();
