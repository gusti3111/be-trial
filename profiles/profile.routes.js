const express = require("express");
const profileRoute = express.Router();
const profileController = require("./profile.controllers");
const profileValidator = require("./profile.validators");
const authentication = require("../middleware/authMiddleware");
const authorization = require("../middleware/protectionMiddleware");

profileRoute.get(
  "/profile/:userId",
  authentication,
  authorization,
  profileController.getProfile
);
profileRoute.put(
  "/profile/:userId",
  authentication,
  authorization,
  profileValidator.bioValidRules(),
  profileController.addProfile
);

module.exports = profileRoute;
