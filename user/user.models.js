const { sequelize, Sequelize } = require("../db/models");
const md5 = require("md5")
const {Op} = require("sequelize")

// // Define table //
const user_games = sequelize.define(
  "user_games",
  {
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING,
  },
  {
    timestamps: true,
  }
);
// // Define table //

class UserModel {
    findByUsername = async (username) => {
      try {
        const data = await user_games.findOne({ where: { username } });
        if (data === null) {
          return null;
        }
        return data.dataValues;
      } catch (error) {
        return error;
      }
    };

    loginWithUsername = async (userUsername, password) => {
      try {
        const data = await user_games.findOne({
          attributes: { exclude: ["password","email"] },
          where: {
            [Op.and]: [{ username: userUsername},{ password: md5(password) }],
          },
        });
        if (data.dataValues.length < 1) {
          return null;
        }
        return data.dataValues;
      } catch (error) {
        return undefined;
      }
    };

  createAccount = async (userUsername, userEmail, userPassword) => {
    try {
      const user_games = sequelize.define(
        "user_games",
        {
          username: Sequelize.STRING,
          email: Sequelize.STRING,
          password: Sequelize.STRING,
        },
        {
          timestamps: true,
        }
      );
     const result= await user_games.create({ username:userUsername,email:userEmail, password: userPassword });
      return result;
    } catch (error) {
      return error;
    }
  };

    findByEmail = async (email) => {
      try {
        const data = await user_games.findOne({ where: { email } });
        if(data === null){
          return null
        }
        return data.dataValues;
      } catch (error) {
        return error;
      }
    };



  }

module.exports = new UserModel();





