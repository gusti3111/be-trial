const express = require("express");
const userRoute = express.Router();
const userController = require("./user.controllers");
const userValidator = require("./user.validators")




userRoute.post("/login", userValidator.loginValidRules(),userController.getLogin);
userRoute.post("/registration",userController.postUserGames);

module.exports = userRoute;


// rollback codingan
// const express = require("express");
// const userRoute = express.Router();
// const userController = require("./user.controllers") 

// userRoute.get("/login", userController.getLogin);

// module.exports = userRoute;
