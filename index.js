const express = require("express");
const app = express();
const port = 3000;
const userRouter = require("./user/user.routes");
const profileRouter = require("./profiles/profile.routes")
const historyRouter = require("./histories/histories.routes")
const roomRouter = require("./rooms/room.routes")
const bodyParser = require('body-parser')
app.use(bodyParser.json())
let cors = require("cors");

app.use(cors());
app.use(express.json());
app.get("/", (req, res) => {
  res.send("Hello, World!");
});
app.use("/", userRouter,profileRouter,historyRouter,roomRouter)

app.listen(port, console.log(`listen port ${port}`));


