const { sequelize, Sequelize } = require("../db/models");

const user_histories = sequelize.define(
  "user_histories",
  {
    status: Sequelize.STRING,
    roomId: Sequelize.INTEGER,
    userId: Sequelize.INTEGER,
  },
  {
    timestamps: true,
  }
);

const user_games = sequelize.define(
  "user_games",
  {
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING,
  },
  {
    timestamps: true,
  }
);

const user_rooms = sequelize.define(
  "user_rooms",
  {
    roomName: Sequelize.STRING,
    player1Id: {
      type: Sequelize.INTEGER,
      references: {
        model: "user_games",
        key: "id",
      },
      allowNull: false,
    },
    player1Choice: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    player2Id: {
      type: Sequelize.INTEGER,
      references: {
        model: "user_games",
        key: "id",
      },
      allowNull: true,
    },
    player2Choice: {
      type: Sequelize.STRING,
      allowNull: true,
    },
  },
  {
    timestamps: true,
  }
);

user_games.hasMany(user_rooms, { foreignKey: "player1Id" });
user_games.hasMany(user_rooms, { foreignKey: "player2Id" });
user_rooms.hasMany(user_histories,{as:"resultGames",foreignKey:"roomId"});
user_histories.belongsTo(user_games, { as: "player", foreignKey: "userId" });
user_histories.belongsTo(user_rooms, { as: "gameRoom", foreignKey: "roomId" });
user_rooms.belongsTo(user_games,{as:"player1Games",foreignKey:"player1Id"});
user_rooms.belongsTo(user_games,{as:"player2Games",foreignKey:"player2Id"});

class RoomModel {
  findByRoomId = async (roomId) => {
    try {
      const result = await user_rooms.findOne({ where: { id: roomId } });
      return result;
    } catch (error) {
      return undefined;
    }
  };

  updateRoom = async (player2Id, player2Choice, roomId) => {
    try {
      await user_rooms.update(
        { player2Id, player2Choice },
        { where: { id: roomId } }
      );
      return true;
    } catch (error) {
      return undefined;
    }
  };

  findAllRooms = async () => {
    try {
      const data = await user_rooms.findAll({
        attributes: [["id", "roomId"], "roomName", "player1Id", "player2Id"],
        include: [
          {
            model: user_histories,
            as: "resultGames",
            attributes: [["userId", "playerId"], "status", "createdAt"],
          },
          {
            model: user_games,
            as: "player1Games",
            attributes: [
              ["id", "player1Id"],
              ["username", "player1Name"],
            ],
          },
          {
            model: user_games,
            as: "player2Games",
            attributes: [
              ["id", "player2Id"],
              ["username", "player2Name"],
            ],
          },
        ],
      });
      return data;
    } catch (error) {
      console.log(error);
      return undefined;
    }
  };

  getOneRoom = async (roomId) => {
    try {
      const data = user_rooms.findOne({
        attributes: [["id", "roomId"], "roomName", "player1Id", "player2Id"],
        include: [
          {
            model: user_histories,
            as: "resultGames",
            attributes: [["userId", "playerId"], "status", "createdAt"],
          },
          {
            model: user_games,
            as: "player1Games",
            attributes: [
              ["id", "player1Id"],
              ["username", "player1Name"],
            ],
          },
          {
            model: user_games,
            as: "player2Games",
            attributes: [
              ["id", "player2Id"],
              ["username", "player2Name"],
            ],
          },
        ],
        where: { id: roomId },
      });
      if (data.length < 1) {
        return null;
      }
      return data;
    } catch (error) {
      return undefined;
    }
  };

  findRoomByRoomName = async (inputRoomName) => {
    try {
      const result = await user_rooms.findOne({
        where: { roomName: inputRoomName },
      });
      return result;
    } catch (error) {
      return undefined;
    }
  };

  createRooms = async (roomName, player1Id, player1Choice) => {
    try {
      await user_rooms.create({ roomName, player1Id, player1Choice });
      return true;
    } catch (error) {
      return false;
    }
  };
}

module.exports = new RoomModel();
