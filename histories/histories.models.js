const { sequelize, Sequelize } = require("../db/models");
const {Op} = require("sequelize")

const user_histories = sequelize.define(
  "user_histories",
  {
    status: Sequelize.STRING,
    roomId: {
      type: Sequelize.INTEGER,
        allowNull:false,
        references:{
          model:"user_rooms",
          key:"id"
        }
    },
    userId: {
      type: Sequelize.INTEGER,
        allowNull:false,
        references:{
          model:"user_games",
          key:"id"
        }
    },
  },
  {
    timestamps: true,
  }
);

const user_games = sequelize.define(
  "user_games",
  {
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING,
  },
  {
    timestamps: true,
  }
);

const user_rooms = sequelize.define(
  "user_rooms",
  {
    roomName: Sequelize.STRING,
    player1Id: {
      type: Sequelize.INTEGER,
      references: {
        model: "user_games",
        key: "id",
      },
      allowNull: false,
    },
    player1Choice: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    player2Id: {
      type: Sequelize.INTEGER,
      references: {
        model: "user_games",
        key: "id",
      },
      allowNull: true,
    },
    player2Choice: {
      type: Sequelize.STRING,
      allowNull: true,
    },
  },
  {
    timestamps: true,
  }
);

user_games.hasMany(user_rooms, { foreignKey: "player1Id" });
user_games.hasMany(user_rooms, { foreignKey: "player2Id" });
user_rooms.hasMany(user_histories,{as:"resultGames",foreignKey:"roomId"});
user_histories.belongsTo(user_games, { as: "player", foreignKey: "userId" });
user_histories.belongsTo(user_rooms, { as: "gameRoom", foreignKey: "roomId" });
user_rooms.belongsTo(user_games,{as:"player1Games",foreignKey:"player1Id"});
user_rooms.belongsTo(user_games,{as:"player2Games",foreignKey:"player2Id"});

class HistoryModel {
  createHistory = async (status, userId) => {
    try {
      await user_histories.create({ status, userId });
      return true;
    } catch (error) {
      console.log(error);
      return error;
    }
  };

  getHistory = async (userId) => {
    try {
      const data = await user_histories.findAll({
        attributes: ["roomId", "status", "createdAt"],
        include: [
          {
            model: user_games,
            as: "player",
            attributes: [
              ["id", "playerId"],
              ["username", "playerName"],
            ],
          },
          {
            model: user_rooms,
            as: "gameRoom",
            attributes: ["roomName"],
          },
        ],
        where: { userId },
      });
      return data;
    } catch (error) {
      return undefined;
    }
  };

  recordHistory = async (status, userId, roomId) => {
    try {
      await user_histories.create({ status, userId, roomId });
      return true;
    } catch (error) {
      return false;
    }
  };

  getHistorybyUserIdAndRoomId = async (player2Id, roomID) => {
    try {
      const data = await user_histories.findOne({
        attributes: [
          ["userId", "playerId"],
          "status",
          ["createdAt", "dateTimeGame"],
        ],
        include: [
          {
            model: user_rooms,
            as: "gameRoom",
            attributes: [["id", "roomId"], "roomName"],
          },
          {
            model: user_games,
            as: "player",
            attributes: [
              ["id", "playerId"],
              ["username", "playerName"],
            ],
          },
        ],
        where: {
          [Op.and]: [{ userId: player2Id.toString() }, { roomId: roomID }],
        },
      });
      return data;
    } catch (error) {
      return undefined;
    }
  };

  findByRoomId = async (roomId) => {
    try {
      const result = await user_histories.findOne({ where: { roomId } });
      return result;
    } catch (error) {
      return undefined;
    }
  };
}

module.exports = new HistoryModel();
