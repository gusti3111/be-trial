const express = require("express");
const historyRoute = express.Router();
const historyController = require("./histories.controllers");
const historyValidator = require("./histories.validators");
const authentication = require("../middleware/authMiddleware");
const authorization = require("../middleware/protectionMiddleware");

historyRoute.get(
  "/history/:userId",
  authentication,
  authorization,
  historyController.getHistory
);
historyRoute.post(
  "/history/:userId",
  authentication,
  authorization,
  historyValidator.historyValidRules(),
  historyController.addHistory
);

module.exports = historyRoute;
